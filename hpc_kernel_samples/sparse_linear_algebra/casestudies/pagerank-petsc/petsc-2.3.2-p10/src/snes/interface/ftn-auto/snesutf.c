#include "petsc.h"
#include "petscfix.h"
/* snesut.c */
/* Fortran interface file */

/*
* This file was generated automatically by bfort from the C source
* file.  
 */

#ifdef PETSC_USE_POINTER_CONVERSION
#if defined(__cplusplus)
extern "C" { 
#endif 
extern void *PetscToPointer(void*);
extern int PetscFromPointer(void *);
extern void PetscRmPointer(void*);
#if defined(__cplusplus)
} 
#endif 

#else

#define PetscToPointer(a) (*(long *)(a))
#define PetscFromPointer(a) (long)(a)
#define PetscRmPointer(a)
#endif

#include "petscsnes.h"
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define snes_ksp_setconvergencetestew_ SNES_KSP_SETCONVERGENCETESTEW
#elif defined(FORTRANDOUBLEUNDERSCORE)
#define snes_ksp_setconvergencetestew_ snes_ksp_setconvergencetestew__
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE)
#define snes_ksp_setconvergencetestew_ snes_ksp_setconvergencetestew
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define snes_ksp_setparametersew_ SNES_KSP_SETPARAMETERSEW
#elif defined(FORTRANDOUBLEUNDERSCORE)
#define snes_ksp_setparametersew_ snes_ksp_setparametersew__
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE)
#define snes_ksp_setparametersew_ snes_ksp_setparametersew
#endif


/* Definitions of Fortran Wrapper routines */
#if defined(__cplusplus)
extern "C" {
#endif
void PETSC_STDCALL   snes_ksp_setconvergencetestew_(SNES snes, int *__ierr ){
*__ierr = SNES_KSP_SetConvergenceTestEW(
	(SNES)PetscToPointer((snes) ));
}
void PETSC_STDCALL   snes_ksp_setparametersew_(SNES snes,PetscInt *version,PetscReal *rtol_0,PetscReal *rtol_max,PetscReal *gamma2,PetscReal *alpha,
                                        PetscReal *alpha2,PetscReal *threshold, int *__ierr ){
*__ierr = SNES_KSP_SetParametersEW(
	(SNES)PetscToPointer((snes) ),*version,*rtol_0,*rtol_max,*gamma2,*alpha,*alpha2,*threshold);
}
#if defined(__cplusplus)
}
#endif
