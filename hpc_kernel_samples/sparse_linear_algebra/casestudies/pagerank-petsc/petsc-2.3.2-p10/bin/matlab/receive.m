function A = receive(port)
%
%   A = receive(port)
%   Receives a matrix from a port opened with openport()
%see openport and closeport
disp('You must build run config/configure.py with the option --with-matlab to build this function');
