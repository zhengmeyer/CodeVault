function port = openport(number)
%
%  port = openport(number)
%  Opens a port to receive matrices from Petsc.
% see closeport and receive
disp('You must build run config/configure.py with the option --with-matlab to build this function');
