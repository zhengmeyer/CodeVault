#!/usr/bin/env python
if __name__ == '__main__':
  import sys
  sys.path.insert(0, '/home/kadir/pagerank/dgleich/codevault/petsc-2.3.2-p10/config')
  import configure
  configure_options = ['--with-shared=0', '-PETSC_ARCH=linux-gnu-c-debug']
  configure.petsc_configure(configure_options)
